import React, { Component } from 'react'
import {
  convertToRaw,
  convertFromRaw,
  EditorState
} from 'draft-js'

import Editor, { composeDecorators } from 'draft-js-plugins-editor'

import createImagePlugin from 'draft-js-image-plugin'

import createAlignmentPlugin from 'draft-js-alignment-plugin'

import createFocusPlugin from 'draft-js-focus-plugin'

import createResizeablePlugin from 'draft-js-resizeable-plugin'

import createBlockDndPlugin from 'draft-js-drag-n-drop-plugin'

import createDragNDropUploadPlugin from '@mikeljames/draft-js-drag-n-drop-upload-plugin'

import imageEditorStyles from './ImageContentEditor.scss'

import mockUpload from './mockUpload'

const focusPlugin = createFocusPlugin()
const resizeablePlugin = createResizeablePlugin()
// const blockDndPlugin = createBlockDndPlugin()
const alignmentPlugin = createAlignmentPlugin()
const { AlignmentTool } = alignmentPlugin

const decorator = composeDecorators(
  resizeablePlugin.decorator,
  alignmentPlugin.decorator,
  focusPlugin.decorator,
  // blockDndPlugin.decorator
)
const imagePlugin = createImagePlugin({ decorator })

// const dragNDropFileUploadPlugin = createDragNDropUploadPlugin({
//   handleUpload: mockUpload,
//   addImage: imagePlugin.addImage,
// })

const plugins = [
  // dragNDropFileUploadPlugin,
  // blockDndPlugin,
  focusPlugin,
  alignmentPlugin,
  resizeablePlugin,
  imagePlugin
]

const initialState = {
  "entityMap": {
      "0": {
          "type": "IMAGE",
          "mutability": "IMMUTABLE",
          "data": {
              "src": "https://osh-developer.s3-ap-southeast-1.amazonaws.com/content_images/items/2167/cdaf6497b68cf7c882a0d703b9c795b4"
          }
      }
  },
  "blocks": [{
      "key": "9gm3s",
      "text": "You can have images in your text field. This is a very rudimentary example, but you can enhance the image plugin with resizing, focus or alignment plugins.",
      "type": "unstyled",
      "depth": 0,
      "inlineStyleRanges": [],
      "entityRanges": [],
      "data": {}
  }, {
      "key": "ov7r",
      "text": " ",
      "type": "atomic",
      "depth": 0,
      "inlineStyleRanges": [],
      "entityRanges": [{
          "offset": 0,
          "length": 1,
          "key": 0
      }],
      "data": {}
  }, {
      "key": "e23a8",
      "text": "See advanced examples further down …",
      "type": "unstyled",
      "depth": 0,
      "inlineStyleRanges": [],
      "entityRanges": [],
      "data": {}
  }]
}

class ImageContentEditor extends Component {
  state = {
    editorState: EditorState.createWithContent(convertFromRaw(initialState))
  }

  onChange = (editorState) => {
    this.setState({
      editorState
    })
  }

  focus = () => {
    this.editor.focus()
  }

  logState = () => {
    const content = this.state.editorState.getCurrentContent()
    console.log(convertToRaw(content))
  }

  render () {
    return (
      <div>
        <div className={'editor'} onClick={this.focus}>
          <Editor editorState={this.state.editorState}
            onChange={this.onChange}
            plugins={plugins}
            ref={(element) => { this.editor = element }}
          />
          <AlignmentTool />
        </div>

        <input onClick={this.logState}
          type='button'
          value='Log State' />
      </div>
    )
  }
}

export default ImageContentEditor
